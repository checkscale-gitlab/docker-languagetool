#!/bin/bash

for v in ${!langtool_*}
do
  config_injected=true
  echo "${v#'langtool_'}=""${!v}" >> config.properties
done

if [ "$config_injected" = true ] ; then
  echo 'The following configuration is passed to LanguageTool:'
	cat config.properties
fi

set -x
java -Xms"${Java_Xms}" -Xmx"${Java_Xmx}" -cp languagetool-server.jar org.languagetool.server.HTTPServer --port "${PORT}" --public --allow-origin '*' --config config.properties
